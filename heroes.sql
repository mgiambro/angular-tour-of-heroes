-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table slim-heroes.heroes: ~2 rows (approximately)
/*!40000 ALTER TABLE `heroes` DISABLE KEYS */;
INSERT INTO `heroes` (`id`, `name`, `date`) VALUES
	(11, 'Mr. Nice', '2010-04-24 00:00:00'),
  (12, 'Narco', '2010-04-24 00:00:00'),
  (13, 'Bombasto', '2010-04-24 00:00:00'),
  (14, 'Celeritas', '2010-04-24 00:00:00'),
  (15, 'Magneta', '2010-04-24 00:00:00'),
  (16, 'RubberMan', '2010-04-24 00:00:00'),
  (17, 'Dynama', '2010-04-24 00:00:00'),
  (18, 'Dr IQ', '2010-04-24 00:00:00'),
  (19, 'Magma', '2010-04-24 00:00:00'),
  (20, 'Tornado', '2010-04-24 00:00:00');
/*!40000 ALTER TABLE `heroes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
